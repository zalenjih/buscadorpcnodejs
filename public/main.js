(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'Software';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-bootstrap-md */ "./node_modules/angular-bootstrap-md/fesm5/angular-bootstrap-md.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _pages_computadoras_computadoras_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pages/computadoras/computadoras.component */ "./src/app/pages/computadoras/computadoras.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _pages_buscadorpc_buscadorpc_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./pages/buscadorpc/buscadorpc.component */ "./src/app/pages/buscadorpc/buscadorpc.component.ts");
/* harmony import */ var _pages_resultados_sugerencia_resultados_sugerencia_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./pages/resultados-sugerencia/resultados-sugerencia.component */ "./src/app/pages/resultados-sugerencia/resultados-sugerencia.component.ts");




// import { AngularFontAwesomeModule } from 'angular-font-awesome';



// HttpClientModule

//Routing 



// Routes
var routes = [
    { path: '', component: _pages_buscadorpc_buscadorpc_component__WEBPACK_IMPORTED_MODULE_9__["BuscadorpcComponent"], pathMatch: 'full' },
    { path: 'computadoras', component: _pages_computadoras_computadoras_component__WEBPACK_IMPORTED_MODULE_6__["ComputadorasComponent"] },
    { path: '**', component: _pages_buscadorpc_buscadorpc_component__WEBPACK_IMPORTED_MODULE_9__["BuscadorpcComponent"] },
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _pages_computadoras_computadoras_component__WEBPACK_IMPORTED_MODULE_6__["ComputadorasComponent"],
                _pages_buscadorpc_buscadorpc_component__WEBPACK_IMPORTED_MODULE_9__["BuscadorpcComponent"],
                _pages_resultados_sugerencia_resultados_sugerencia_component__WEBPACK_IMPORTED_MODULE_10__["ResultadosSugerenciaComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_3__["MDBBootstrapModule"].forRoot(),
                _angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterModule"].forRoot(routes),
                _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"],
                // AngularFontAwesomeModule
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["NO_ERRORS_SCHEMA"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/pages/buscadorpc/buscadorpc.component.html":
/*!************************************************************!*\
  !*** ./src/app/pages/buscadorpc/buscadorpc.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container animated fadeIn\" *ngIf=\"formulario\">\n    <div class=\"row\">\n        <div class=\"mt-12 col-sm-12 col-md-10 col-lg-7 centrar-div\">\n\n\n            <mdb-card>\n                <!--Card image-->\n                <mdb-card-img src=\"assets/img/home.jpg\" alt=\"Card image cap\"></mdb-card-img>\n                <!--Card content-->\n                <mdb-card-body>\n\n                    <!--Title-->\n                    <mdb-card-title cascade=\"true\" class=\"text-center\">\n                        <h2>\n                            <strong>Bienvenido</strong>\n                        </h2>\n                        <h5 class=\"indigo-text\">\n                            <strong>Tu computadora en un clic</strong>\n                        </h5>\n                    </mdb-card-title>\n\n                    <!--Text-->\n                    <mdb-card-text cascade=\"true\" class=\"text-center\">\n                        <button type=\"button\" class=\"btn btn-secondary waves-effect waves-light\" data-toggle=\"modal\" data-target=\"#exampleModalLongSC\">Empezar</button>\n                    </mdb-card-text>\n\n                    <!-- <a href=\"#\" mdbBtn color=\"primary\" mdbWavesEffect>Button</a> -->\n                </mdb-card-body>\n            </mdb-card>\n\n\n        </div>\n    </div>\n</div>\n\n<div class=\"container animated fadeIn\" *ngIf=\"!formulario\">\n    <div class=\"row\">\n        <div class=\"mt-5 col-12 col-sm-12 col-md-4 col-lg-4\">\n            <mdb-card *ngFor=\"let computadora of computadoras;let i = index\">\n                <!--Card image-->\n                <mdb-card-img [src]=\"computadora.img\" alt=\"Card image cap\"></mdb-card-img>\n                <!--Card content-->\n                <mdb-card-body>\n\n                    <!--Title-->\n                    <mdb-card-title cascade=\"true\" class=\"text-center\">\n                        <h2>\n                            <strong> {{i + 1}}) {{computadora.marca}} {{ computadora.modelo }}</strong>\n                        </h2>\n                        <h5 class=\"indigo-text\">\n                            <strong>Propiedades:</strong>\n                        </h5>\n                    </mdb-card-title>\n\n                    <!--Text-->\n                    <mdb-card-text cascade=\"true\" class=\"text-center\">\n                        <!-- <button type=\"button\" class=\"btn btn-secondary waves-effect waves-light\" data-toggle=\"modal\" data-target=\"#exampleModalLongSC\">Empezar</button> -->\n                    </mdb-card-text>\n\n                    <!-- <a href=\"#\" mdbBtn color=\"primary\" mdbWavesEffect>Button</a> -->\n                </mdb-card-body>\n            </mdb-card>\n        </div>\n    </div>\n</div>\n\n\n<!-- SEGUNDA VENTANA -->\n<div class=\"modal fade\" id=\"exampleModalLongSC\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLongTitleSC\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-dialog-scrollable\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h5 class=\"modal-title\" id=\"exampleModalLongTitleSC\">Modal title</h5>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">×</span>\n        </button>\n            </div>\n            <div class=\"modal-body\">\n                <h2 class=\"text-center\">A que te dedicas</h2>\n                <div class=\"centrar\" >\n\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"Estudiante\" (change)=\"agregarOcupacion($event)\" >\n                        <label class=\"custom-control-label\" for=\"Estudiante\">Estudiante</label>\n                    </div>\n\n                    <div class=\"custom-control custom-switch\" >\n                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"Diseñador\" (change)=\"agregarOcupacion($event)\">\n                        <label class=\"custom-control-label\" for=\"Diseñador\">Diseñador</label>\n                    </div>\n\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"Secretario\" (change)=\"agregarOcupacion($event)\">\n                        <label class=\"custom-control-label\" for=\"Secretario\">Secretario</label>\n                    </div>\n\n\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"Desarrollador\" (change)=\"agregarOcupacion($event)\">\n                        <label class=\"custom-control-label\" for=\"Desarrollador\">Desarrollador</label>\n                    </div>\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"Gamer\" (change)=\"agregarOcupacion($event)\">\n                        <label class=\"custom-control-label\" for=\"Gamer\">Gamer</label>\n                    </div>\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"Todologo\" (change)=\"agregarOcupacion($event)\">\n                        <label class=\"custom-control-label\" for=\"Todologo\">Todologo</label>\n                    </div>\n                </div>\n\n                <br>\n\n                <h2 class=\"text-align: justify\">Cuantas horas le dara de uso</h2>\n                <div class=\"centrar\">\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"radio\" class=\"custom-control-input\" id=\"1_o_menos\" name=\"groupOfDefaultRadios\" (change)=\"agregarHora($event)\">\n                        <label class=\"custom-control-label\" for=\"1_o_menos\">1 o menos</label>\n                    </div>\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"radio\" class=\"custom-control-input\" id=\"1-4\" name=\"groupOfDefaultRadios\" (change)=\"agregarHora($event)\">\n                        <label class=\"custom-control-label\" for=\"1-4\">1-4</label>\n                    </div>\n\n                    <!-- Group of default radios - option 2 -->\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"radio\" class=\"custom-control-input\" id=\"4-8\" name=\"groupOfDefaultRadios\" (change)=\"agregarHora($event)\">\n                        <label class=\"custom-control-label\" for=\"4-8\">4-8</label>\n                    </div>\n\n                    <!-- Group of default radios - option 3 -->\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"radio\" class=\"custom-control-input\" id=\"8-12\" name=\"groupOfDefaultRadios\" (change)=\"agregarHora($event)\">\n                        <label class=\"custom-control-label\" for=\"8-12\">8-12</label>\n                    </div>\n\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"radio\" class=\"custom-control-input\" id=\"12_o_mas\" name=\"groupOfDefaultRadios\" (change)=\"agregarHora($event)\">\n                        <label class=\"custom-control-label\" for=\"12_o_mas\">12 o mas</label>\n                    </div>\n                </div>\n\n                <br>\n\n                <h2 class=\"text-center\">Presupuesto</h2>\n                <div class=\"centrar\">\n                    <input required type=\"number\" name=\"Presupuesto\" [(ngModel)]=\"preferencias.presupuesto\" class=\"form-control mb-4\" placeholder=\"Presupuesto\" />\n                </div>\n\n                <br>\n                \n                <h2 class=\"text-center\">Nivel de uso que le dara</h2>\n                <div class=\"centrar\">\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"radio\" class=\"custom-control-input\" id=\"Bajo\" name=\"groupOfDefaultRadios\" (change)=\"agregarUso($event)\">\n                        <label class=\"custom-control-label\" for=\"Bajo\">Bajo</label>\n                    </div>\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"radio\" class=\"custom-control-input\" id=\"Bajo-Medio\" name=\"groupOfDefaultRadios\" (change)=\"agregarUso($event)\">\n                        <label class=\"custom-control-label\" for=\"Bajo-Medio\">Bajo-Medio</label>\n                    </div>\n\n                    <!-- Group of default radios - option 2 -->\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"radio\" class=\"custom-control-input\" id=\"Medio\" name=\"groupOfDefaultRadios\" (change)=\"agregarUso($event)\">\n                        <label class=\"custom-control-label\" for=\"Medio\">Medio</label>\n                    </div>\n\n                    <!-- Group of default radios - option 3 -->\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"radio\" class=\"custom-control-input\" id=\"Medio-Alto\" name=\"groupOfDefaultRadios\" (change)=\"agregarUso($event)\">\n                        <label class=\"custom-control-label\" for=\"Medio-Alto\">Medio-Alto</label>\n                    </div>\n\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"radio\" class=\"custom-control-input\" id=\"Alto\" name=\"groupOfDefaultRadios\" (change)=\"agregarUso($event)\">\n                        <label class=\"custom-control-label\" for=\"Alto\">Alto</label>\n                    </div>\n                </div>\n\n                <br>\n\n                <h2 class=\"text-center\">Edad</h2>\n                <div class=\"centrar\">\n                    <input required type=\"number\" [(ngModel)]=\"preferencias.edad\" name=\"Edad\" class=\"form-control mb-4\" placeholder=\"Edad\" />\n                </div>\n\n                <br>\n\n                <h2 class=\"text-center\">Que tipo de uso le da regularmente</h2>\n                <!-- <div class=\"centrar\"> -->\n                <input required type=\"text\" name=\"uso\" class=\"form-control mb-4\" placeholder=\"Uso\" />\n                <!-- </div> -->\n\n                <h2 class=\"text-center\">Marca de equipo preferida</h2>\n                <div class=\"centrar\">\n\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"HP\" (change)=\"agregarMarcaPreferencia($event)\">\n                        <label class=\"custom-control-label\" for=\"HP\">Hp</label>\n                    </div>\n\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"DELL\" (change)=\"agregarMarcaPreferencia($event)\">\n                        <label class=\"custom-control-label\" for=\"DELL\">Dell</label>\n                    </div>\n\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"TOSHIBA\" (change)=\"agregarMarcaPreferencia($event)\">\n                        <label class=\"custom-control-label\" for=\"TOSHIBA\">Toshiba</label>\n                    </div>\n\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"ASUS\" (change)=\"agregarMarcaPreferencia($event)\">\n                        <label class=\"custom-control-label\" for=\"ASUS\">Asus</label>\n                    </div>\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"LENOVO\" (change)=\"agregarMarcaPreferencia($event)\">\n                        <label class=\"custom-control-label\" for=\"LENOVO\">Lenovo</label>\n                    </div>\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"SAMSUNG\" (change)=\"agregarMarcaPreferencia($event)\">\n                        <label class=\"custom-control-label\" for=\"SAMSUNG\">Samsung</label>\n                    </div>\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"APPLE\" (change)=\"agregarMarcaPreferencia($event)\">\n                        <label class=\"custom-control-label\" for=\"APPLE\">Apple</label>\n                    </div>\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"ACER\" (change)=\"agregarMarcaPreferencia($event)\">\n                        <label class=\"custom-control-label\" for=\"ACER\">Acer</label>\n                    </div>\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"ALIENWARE\" (change)=\"agregarMarcaPreferencia($event)\">\n                        <label class=\"custom-control-label\" for=\"ALIENWARE\">Alienware</label>\n                    </div>\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"GATEWAY\" (change)=\"agregarMarcaPreferencia($event)\">\n                        <label class=\"custom-control-label\" for=\"GATEWAY\">Gateway</label>\n                    </div>\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"LANIX\" (change)=\"agregarMarcaPreferencia($event)\">\n                        <label class=\"custom-control-label\" for=\"LANIX\">Lanix</label>\n                    </div>\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"OTRA\" (change)=\"agregarMarcaPreferencia($event)\">\n                        <label class=\"custom-control-label\" for=\"OTRA\">Otra</label>\n                    </div>\n                    <div class=\"custom-control custom-switch\">\n                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"NINGUNA\" (change)=\"agregarMarcaPreferencia($event)\">\n                        <label class=\"custom-control-label\" for=\"NINGUNA\">Ninguna</label>\n                    </div>\n                </div>\n\n                <br>\n\n                <h2 class=\"text-center\">Seccion de caracteristicas</h2>\n\n                <br>\n\n                <h2 class=\"text-center\">Color que le gustaria</h2>\n                <div class=\"centrar\">\n                    <input required type=\"text\" name=\"Color\" class=\"form-control mb-4\" placeholder=\"Color\" [(ngModel)]=\"preferencias.color\"/>\n                </div>\n                <br>\n                <!-- <h2 class=\"text-center\">Tamaño de la pantalla</h2>\n                <div class=\"centrar\">\n                    <input required type=\"text\" name=\"Tamaño\" class=\"form-control mb-4\" placeholder=\"Tamaño\" />\n                </div> -->\n                <br>\n                <h2 class=\"text-center\">Cantidad de almacenamiento</h2>\n                <div class=\"centrar\">\n                    <input required type=\"text\" name=\"almacenamiento\" class=\"form-control mb-4\" placeholder=\"Almacenamiento\" [(ngModel)]=\"preferencias.almacenamiento\"/>\n                </div>\n                <br>\n                <!-- <h2 class=\"text-center\">Tamaño del equipo</h2>\n                <div class=\"centrar\">\n                    <input required type=\"number\" name=\"tamanoequipo\" class=\"form-control mb-4\" placeholder=\"Tamaño Del equipo\" [(ngModel)]=\"preferencias.ta\"/>\n                </div> -->\n\n                <div class=\"modal-footer rem-dimensions\">\n                    <button type=\"button\" class=\"btn btn-secondary waves-effect waves-light\" data-dismiss=\"modal\">Cancelar</button>\n                    <button type=\"button\" class=\"btn btn-primary waves-effect waves-light\" data-dismiss=\"modal\" (click)=\"sugerirPc()\">Procesar</button>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/buscadorpc/buscadorpc.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/pages/buscadorpc/buscadorpc.component.ts ***!
  \**********************************************************/
/*! exports provided: BuscadorpcComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuscadorpcComponent", function() { return BuscadorpcComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_computadora_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/computadora.service */ "./src/app/services/computadora.service.ts");



var BuscadorpcComponent = /** @class */ (function () {
    /** */
    // tslint:disable-next-line: jsdoc-format
    /** buscar la forma en que se marquen varios y guardarlos*/
    // tslint:disable-next-line: variable-name
    function BuscadorpcComponent(_computadoraService) {
        this._computadoraService = _computadoraService;
        // tslint:disable-next-line: jsdoc-format
        /**variables que utilizare para captar todo lo que se marque */
        this.formulario = true;
        this.computadoras = [];
        this.arregloOcupacion = [];
        this.arregloMarcaPreferencia = [];
        this.desde = 0;
        this.preferencias = {
            ocupacion: '',
            horas: 0,
            presupuesto: 0,
            edad: 0,
            nivelUso: '',
            marcaPreferencia: '',
            color: '',
            almacenamiento: ''
        };
    }
    BuscadorpcComponent.prototype.ngOnInit = function () {
        this.arregloOcupacion = [];
        this.arregloMarcaPreferencia = [];
    };
    BuscadorpcComponent.prototype.sugerirPc = function ( /*todos los argumentos con los que voy a validar */) {
        // tslint:disable-next-line: jsdoc-format
        /**referencia al donde mandare los parametros */
        /* this._computadoraService.cargarConAlgoritmo() */
        var _this = this;
        this._computadoraService.cargarConAlgoritmo(this.preferencias, this.desde).subscribe(function (resp) {
            _this.computadoras = resp;
            console.log(_this.computadoras);
        });
        if (this.formulario) {
            this.formulario = false;
        }
        else {
            this.formulario = true;
        }
    };
    BuscadorpcComponent.prototype.agregarOcupacion = function (event) {
        console.log(event);
        if (event.target.checked) {
            this.arregloOcupacion.push(event.target.id.toString());
        }
        else {
            for (var i = 0; i < this.arregloOcupacion.length; i++) {
                if (this.arregloOcupacion[i] === event.target.id.toString()) {
                    this.arregloOcupacion.splice(i, 1);
                }
            }
        }
        // console.log(this.arregloOcupacion);
        this.preferencias.ocupacion = this.arregloOcupacion.toString();
        // console.log(this.preferencias.ocupacion);
        // console.log(this.preferencias.presupuesto);
        // console.log(this.preferencias.horas);
    };
    BuscadorpcComponent.prototype.agregarMarcaPreferencia = function (event) {
        if (event.target.checked) {
            this.arregloMarcaPreferencia.push(event.target.id.toString());
        }
        else {
            for (var i = 0; i < this.arregloMarcaPreferencia.length; i++) {
                if (this.arregloMarcaPreferencia[i].toString === event.target.id.toString()) {
                    this.arregloMarcaPreferencia.splice(i, i);
                    // this.arregloVideo.;
                }
            }
        }
        this.preferencias.marcaPreferencia = this.arregloMarcaPreferencia.toString();
        // console.log(this.preferencias.marcaPreferencia);
        // console.log(this.preferencias.color);
    };
    BuscadorpcComponent.prototype.agregarHora = function (event) {
        if (event.target.checked) {
            this.preferencias.horas = event.target.id.toString();
        }
        // console.log(this.preferencias.horas);
    };
    BuscadorpcComponent.prototype.agregarUso = function (event) {
        if (event.target.checked) {
            this.preferencias.nivelUso = event.target.id.toString();
        }
        // console.log(this.preferencias.nivelUso);
    };
    BuscadorpcComponent.prototype.onclick = function () {
        this.clickMessage = 'You are my hero!';
        // console.log(this.clickMessage);
        // console.log(this.preferencias.edad);
        // console.log(this.preferencias.horas);
        console.log(this.preferencias);
    };
    BuscadorpcComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-buscadorpc',
            template: __webpack_require__(/*! ./buscadorpc.component.html */ "./src/app/pages/buscadorpc/buscadorpc.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_computadora_service__WEBPACK_IMPORTED_MODULE_2__["ComputadoraService"]])
    ], BuscadorpcComponent);
    return BuscadorpcComponent;
}());



/***/ }),

/***/ "./src/app/pages/computadoras/computadoras.component.html":
/*!****************************************************************!*\
  !*** ./src/app/pages/computadoras/computadoras.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <!-- Default form login -->\n    <form ngNativeValidate #f=\"ngForm\" (ngSubmit)=\"agregarPc(f)\" class=\"text-center border border-light p-5\">\n        <h2 class=\"mb-4 text-center\">Agregar computadora</h2>\n\n        <p class=\"h5 mb-2 text-left\">Datos principales</p>\n        <!-- PRIMER FILA -->\n        <div class=\"row\">\n            <!-- MARCA -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <input required type=\"text\" name=\"marca\" [(ngModel)]=\"computadora.marca\" class=\"form-control mb-4\" placeholder=\"Marca\" />\n            </div>\n\n            <!-- MODELO -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <input required type=\"text\" name=\"modelo\" [(ngModel)]=\"computadora.modelo\" class=\"form-control mb-4\" placeholder=\"Modelo\" />\n            </div>\n            <!-- PRECIO -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <input required type=\"Number\" name=\"precio\" [(ngModel)]=\"computadora.precio\" class=\"form-control mb-4\" placeholder=\"Precio\" />\n            </div>\n        </div>\n        <!-- SEGUNDA FILA -->\n        <div class=\"row\">\n            <!-- CAPACIDAD DE ALMACENAMIENTO -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <input required type=\"text\" name=\"almacenamiento\" [(ngModel)]=\"computadora.almacenamiento\" class=\"form-control mb-4\" placeholder=\"Capacidad De Almacenamiento\" />\n            </div>\n            <!-- UNIDAD DE MEDIDA ALMACENAMIENTO -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <select required name=\"tipoAlmacenamineto\" [(ngModel)]=\"computadora.tipoAlmacenamineto\" class=\"browser-default custom-select mb-4\" placeholder=\"Tipo de almacenamiento\">\n                    <option selected=\"true\" disabled value=\"default\" >Tipo de almacenamiento</option>\n                    <option value=\"GIGABYTE\">GIGABYTE</option>\n                    <option value=\"TERABYTE\">TERABYTE</option>\n                </select>\n            </div>\n            <!-- MEMORIA RAM -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <input required type=\"text\" name=\"ram\" [(ngModel)]=\"computadora.ram\" class=\"form-control mb-4\" placeholder=\"Ram\" />\n            </div>\n        </div>\n        <!-- TERCER FILA -->\n        <div class=\"row\">\n            <!-- VELOCIDAD CPU -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <input required type=\"text\" name=\"velocidadCpu\" [(ngModel)]=\"computadora.velocidadCpu\" class=\"form-control mb-4\" placeholder=\"Velocidad Del Cpu\" />\n            </div>\n            <!-- MARCA CPU -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <select required name=\"marcaCpu\" [(ngModel)]=\"computadora.marcaCpu\" class=\"browser-default custom-select mb-4\" placeholder=\"Seleccionar una marca de CPU\">\n          <option selected=\"true\" disabled value=\"default\" >Seleccionar una marca de CPU</option>\n          <option value=\"INTEL\">Intel</option>\n          <option value=\"AMD\">Amd</option>\n        </select>\n            </div>\n            <!-- LECTOR DE CD -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <select required name=\"lectorCD\" [(ngModel)]=\"computadora.lectorCD\" class=\"browser-default custom-select mb-4\">\n          <option selected=\"true\"  value=\"default\" disabled >¿Tiene lector de CD?</option>\n          <option value=\"true\">Si</option>\n          <option value=\"false\">No</option>\n        </select>\n            </div>\n        </div>\n        <!-- CUARTA FILA -->\n        <div class=\"row\">\n            <!-- AQUI VA ENTRADAS DE VIDEO (CAMBIAR) -->\n            <!-- TARJETA INTEGRADA -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <select required name=\"tarjetaIntegrada\" [(ngModel)]=\"computadora.tajetaIntegrada\" class=\"browser-default custom-select mb-4\">\n          <option selected=\"true\" disabled value=\"default\">¿Tiene tarjeta integrada?</option>\n          <option value=\"true\">Si</option>\n          <option value=\"false\">No</option>\n        </select>\n            </div>\n            <!-- TARJETA DEDICADA -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <select required name=\"tarjetaDedicada\" [(ngModel)]=\"computadora.tajetaDedicada\" class=\"browser-default custom-select mb-4\">\n          <option selected=\"true\" disabled value=\"default\">¿Tiene tarjeta Dedicada?</option>\n          <option value=\"true\">Si</option>\n          <option value=\"false\">No</option>\n        </select>\n            </div>\n            <!-- MODELO TARJETA DE VIDEO -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <input required type=\"text\" name=\"modeloTarjetaVideo\" [(ngModel)]=\"computadora.modeloTarjetaVideo\" class=\"form-control mb-4\" placeholder=\"Tarjeta integrada\" />\n            </div>\n        </div>\n        <!-- QUINTA FILA -->\n        <div class=\"row\">\n            <!-- RESOLUCION -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <input required type=\"text\" name=\"resolucion\" [(ngModel)]=\"computadora.resolucion\" class=\"form-control mb-4\" placeholder=\"resolucion\" />\n            </div>\n\n            <!-- ANCHO -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <input required type=\"text\" name=\"ancho\" [(ngModel)]=\"computadora.ancho\" class=\"form-control mb-4\" placeholder=\"ancho\" />\n            </div>\n            <!-- ALTO -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <input required type=\"text\" name=\"alto\" [(ngModel)]=\"computadora.alto\" class=\"form-control mb-4\" placeholder=\"alto\" />\n            </div>\n        </div>\n\n        <!-- SEXTA FILA -->\n        <div class=\"row\">\n            <!-- PESO -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <input required type=\"text\" name=\"peso\" [(ngModel)]=\"computadora.peso\" class=\"form-control mb-4\" placeholder=\"peso\" />\n            </div>\n            <!-- USB2 -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <input required type=\"text\" name=\"usb2\" [(ngModel)]=\"computadora.usb2\" class=\"form-control mb-4\" placeholder=\"usb2\" />\n            </div>\n            <!-- USB3 -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <input required type=\"text\" name=\"usb3\" [(ngModel)]=\"computadora.usb3\" class=\"form-control mb-4\" placeholder=\"usb3\" />\n            </div>\n        </div>\n\n        <!-- SEPTIMA FILA -->\n        <div class=\"row\">\n            <!-- ranura de expansion -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <select required name=\"expansionRam\" [(ngModel)]=\"computadora.expansionRam\" class=\"browser-default custom-select mb-4\">\n                  <option selected=\"true\" disabled value=\"default\">¿Tiene ranura de expansion?</option>\n                  <option value=\"true\">Si</option>\n                  <option value=\"false\">No</option>\n                </select>\n            </div>\n            <!-- tamaño de la pantalla -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <!-- <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\"> -->\n                <input required type=\"text\" name=\"tamanoPantalla\" [(ngModel)]=\"computadora.tamanoPantalla\" class=\"form-control mb-4\" placeholder=\"tamanoPantalla\" />\n                <!-- </div> -->\n            </div>\n            <!-- sistema operativo -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <select required name=\"sistemaOperativo\" [(ngModel)]=\"computadora.sistemaOperativo\" class=\"browser-default custom-select mb-4\">\n                <option selected=\"true\" disabled value=\"default\">Sistema operativo cargado</option>\n                <option value=\"WINDOWS10PRO\">Windows 10 Pro</option>\n                <option value=\"WINDOWS10HOME\">Windows 10 Home</option>\n                <option value=\"WINDOWS10SL\">Windows 10 Single Lenguaje</option>\n                <option value=\"DISTLINUX\">Distro Linux</option>\n                <option value=\"MACOSX\">MACOSX</option>\n                <option value=\"MACOSC\">MACOSC</option>\n              </select>\n            </div>\n        </div>\n        <!-- octava fila -->\n        <div class=\"row\">\n            <!-- ranura de expansion -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n                <input required type=\"text\" name=\"color\" [(ngModel)]=\"computadora.color\" class=\"form-control mb-4\" placeholder=\"Color\" />\n            </div>\n            <!-- tamaño de la pantalla -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n            </div>\n            <!-- sistema operativo -->\n            <div class=\"col-12 col-sm-12 col-md-4 col-lg-4\">\n\n            </div>\n        </div>\n\n        <div class=\"mb-4 bod border border-light p-5\">\n            <input required type=\"text\" name=\"video\" [ngModel]=\"computadora.video\" class=\"form-control\" placeholder=\"Salida de video\" disabled />\n            <div class=\"text-justify centrar\">\n\n                <div class=\"custom-control custom-checkbox\">\n                    <input type=\"checkbox\" class=\"custom-control-input\" id=\"VGA\" (change)=\"agregarVideo($event)\" />\n                    <label class=\"custom-control-label\" for=\"VGA\">VGA</label>\n                </div>\n                <div class=\"custom-control custom-checkbox\">\n                    <input type=\"checkbox\" class=\"custom-control-input\" id=\"S-VIDEO\" (change)=\"agregarVideo($event)\" />\n                    <label class=\"custom-control-label\" for=\"S-VIDEO\">S-VIDEO</label>\n                </div>\n                <div class=\"custom-control custom-checkbox\">\n                    <input type=\"checkbox\" class=\"custom-control-input\" id=\"HDMI\" (change)=\"agregarVideo($event)\" />\n                    <label class=\"custom-control-label\" for=\"HDMI\">HDMI</label>\n                </div>\n                <div class=\"custom-control custom-checkbox\">\n                    <input type=\"checkbox\" class=\"custom-control-input\" id=\"MINI_VGA\" (change)=\"agregarVideo($event)\" />\n                    <label class=\"custom-control-label\" for=\"MINI_VGA\">MINI VGA</label>\n                </div>\n                <div class=\"custom-control custom-checkbox\"> \n                    <input type=\"checkbox\" class=\"custom-control-input\" id=\"DISPLAY_PORT\" (change)=\"agregarVideo($event)\" />\n                    <label class=\"custom-control-label\" for=\"DISPLAY_PORT\">DISPLAY PORT</label>\n                </div>\n            </div>\n        </div>\n        <!-- Sign in button -->\n        <button class=\"btn btn-info btn-block my-4\" type=\"submit\">Agregar</button>\n\n    </form>\n    <!-- Default form login -->\n</div>"

/***/ }),

/***/ "./src/app/pages/computadoras/computadoras.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/pages/computadoras/computadoras.component.ts ***!
  \**************************************************************/
/*! exports provided: ComputadorasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComputadorasComponent", function() { return ComputadorasComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_computadora_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/computadora.service */ "./src/app/services/computadora.service.ts");



// import { post } from 'selenium-webdriver/http';
var ComputadorasComponent = /** @class */ (function () {
    // tslint:disable-next-line: variable-name
    function ComputadorasComponent(_computadoraService) {
        this._computadoraService = _computadoraService;
        this.arregloComputadorasGuardadas = [];
        this.arregloVideo = [];
        this.desde = 0;
        this.computadora = {
            marca: '',
            modelo: '',
            precio: 0.0,
            color: '',
            almacenamiento: '',
            tipoAlmacenamineto: 'default',
            ram: '',
            velocidadCpu: '',
            marcaCpu: 'default',
            lectorCD: 'default',
            video: '',
            tajetaIntegrada: 'default',
            tajetaDedicada: 'default',
            modeloTarjetaVideo: '',
            tamanoPantalla: '',
            resolucion: '',
            ancho: '',
            alto: '',
            peso: '',
            sistemaOperativo: 'default',
            usb2: '',
            usb3: '',
            expansionRam: 'default',
            img: ''
        };
    }
    ComputadorasComponent.prototype.ngOnInit = function () {
        this.arregloVideo = [];
    };
    ComputadorasComponent.prototype.agregarVideo = function (event) {
        if (event.target.checked) {
            this.arregloVideo.push(event.target.id.toString());
        }
        else {
            for (var i = 0; i < this.arregloVideo.length; i++) {
                if (this.arregloVideo[i] === event.target.id.toString()) {
                    this.arregloVideo.splice(i, 1);
                    // this.arregloVideo.;|
                }
            }
        }
        this.computadora.video = this.arregloVideo.toString();
        console.log(this.arregloVideo);
        console.log(this.computadora.video);
    };
    // tslint:disable-next-line: jsdoc-format
    /**una forma de validacion para la captura de datos o por si algo es invalido */
    ComputadorasComponent.prototype.agregarPc = function (form) {
        if (form.invalid) {
            return;
        }
        console.log(this.computadora);
        // tslint:disable-next-line: jsdoc-format
        /**manda a llamar al metodo para guardar la computadora */
        this._computadoraService.registrarComputadora(this.computadora).subscribe(function (resp) {
            console.log('se guardo exitosamente: computadoras.components.ts');
        });
        /*mensja o notificacion estilo retro */
        /* swal.fire({
          title: '¿Cargar computadora?',
          text: "Aquí van los datos ordenados de la computadora",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Cancelar',
          confirmButtonText: 'Si, ¡Agrégala!'
        }).then((result) => {
          if (result.value) {
            swal.fire(
              '¡Agregada!',
              'La computadora ha sido guardada correctamente',
              'success'
            )
          }
        })
     */
        /*retro */
    };
    ComputadorasComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-computadoras',
            template: __webpack_require__(/*! ./computadoras.component.html */ "./src/app/pages/computadoras/computadoras.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_computadora_service__WEBPACK_IMPORTED_MODULE_2__["ComputadoraService"]])
    ], ComputadorasComponent);
    return ComputadorasComponent;
}());



/***/ }),

/***/ "./src/app/pages/resultados-sugerencia/resultados-sugerencia.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/resultados-sugerencia/resultados-sugerencia.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  resultados-sugerencia works!\n</p>\n"

/***/ }),

/***/ "./src/app/pages/resultados-sugerencia/resultados-sugerencia.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/pages/resultados-sugerencia/resultados-sugerencia.component.ts ***!
  \********************************************************************************/
/*! exports provided: ResultadosSugerenciaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultadosSugerenciaComponent", function() { return ResultadosSugerenciaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var ResultadosSugerenciaComponent = /** @class */ (function () {
    function ResultadosSugerenciaComponent(router, activatedRoute) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.computadoras = [];
        this.activatedRoute.params.subscribe(function (params) {
            console.log(params);
        });
    }
    ResultadosSugerenciaComponent.prototype.ngOnInit = function () {
    };
    ResultadosSugerenciaComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-resultados-sugerencia',
            template: __webpack_require__(/*! ./resultados-sugerencia.component.html */ "./src/app/pages/resultados-sugerencia/resultados-sugerencia.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], ResultadosSugerenciaComponent);
    return ResultadosSugerenciaComponent;
}());



/***/ }),

/***/ "./src/app/services/computadora.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/computadora.service.ts ***!
  \*************************************************/
/*! exports provided: ComputadoraService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComputadoraService", function() { return ComputadoraService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");







var ComputadoraService = /** @class */ (function () {
    function ComputadoraService(_http, router) {
        this._http = _http;
        this.router = router;
        this.URL_SERVICIOS = 'https://buscadorpc-app.herokuapp.com/api';
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set('Content-Type', 'application/json');
    }
    // ==============================================================
    // Cargar computadoras - GET
    // ==============================================================
    ComputadoraService.prototype.cargarComputadoras = function (desde) {
        if (desde === void 0) { desde = 0; }
        var url = this.URL_SERVICIOS + '/computadora?desde=' + desde;
        return this._http.get(url, { headers: this.headers });
    };
    // ==============================================================
    // Cargar computadoras - GET
    // ==============================================================
    ComputadoraService.prototype.cargarConAlgoritmo = function (preferencias, desde) {
        /*aqui mando a llamar a la ruta del backend server*/
        var url = this.URL_SERVICIOS + '/computadora/sugerencia';
        // http://localhost:3000/computadora/sugerencia?desde=0&presupuesto=15252.2&edad=15&horas=12&ocupacion=estudiante, desarrollador, diseñador
        return this._http.post(url, preferencias, { headers: this.headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (resp) {
            return resp.computadoras;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire('Error' + err.status, err.error.mensaje, 'error');
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["throwError"])(err);
        }));
    };
    // ==============================================================
    // Registrar computadora - POST
    // ==============================================================
    ComputadoraService.prototype.registrarComputadora = function (computadora) {
        var url = this.URL_SERVICIOS + '/computadora';
        return this._http.post(url, computadora, { headers: this.headers }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (resp) {
            console.log(resp);
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire('Computadora registrada', 'Computadora registrada correctamente', 'success');
            return true;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire('Error' + err.status, err.error.mensaje, 'error');
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["throwError"])(err);
        }));
    };
    ComputadoraService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ComputadoraService);
    return ComputadoraService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Humberto Zapata\Documents\Programación\Proyectos con api rest\buscadorpcangular\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map