// Requires
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
const http = require('http');

// Inicializar variables
const app = express();
/* const CONEXION = 'mongodb://localhost:27017/buscadorpc'; */
const CONEXION = 'mongodb+srv://user:<cqazcHLMttlQW36x>@cluster0-bqdaq.mongodb.net/test?retryWrites=true';
// contraseña usuario db O98mKOrYIdtGPklU

// CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
    next();
}); 

// Body-Parser
// Parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Conexión a la base de datos
require('./databse');

/* mongoose.connection.openUri(CONEXION, { useNewUrlParser: true }, (err, res) => {
    if (err) throw err;
    console.log('Conexión a la base de datos: \x1b[33m%s\x1b[0m', 'online');
}); */




// Importando rutas
// const appRoutes = require('./Routes/app');
const computadorasRoutes = require('./Routes/computadora');


// Angular DIST output forlder
app.use(express.static(path.join(__dirname, 'public')));

// Rutas
app.use('/api/computadora', computadorasRoutes);

// Enviar las peticiones a la aplicación de angular
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/index.html'));
});

// Configurando puerto dinámico
const port = process.env.PORT || '3000';
app.set('port', port);

const server = http.createServer(app);

// Escuchar peticiones
server.listen(app.get('port'), () => {
    console.log('Express server en el puerto ' + port + ' : \x1b[36m%s\x1b[0m', 'online');
});